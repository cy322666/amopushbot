<?php

/**
 * Created by PhpStorm.
 * User: cy322666
 * Date: 08.02.2019
 * Time: 13:52
 *
 */

/*
 * Библиотека для телеграм бота
 */

/**
 * Может:
 *
 * # Пушить сообщение
 * # Пушить сообщение с кнопкой
 * # Пушить меню
 *
 * @TOKEN           - токен бота из config.php
 * @param $chat_id  - id чата
 * @param $msg      - передаваемое сообщение
 * @param $link     - url при нажатии кнопки в случае передачи
 * @param $text     - текст на кнопке
 * @param $menu     - массив для отображения меню
 *
 * Пушит в указанный чат текст
 *
 */
function sendMessage($chat_id, $msg, $link, $text, $reply)
{
    if ($link)
    {
        $inButt = [
            "text" => $text,
            "url"  => $link
        ];
        $inline_keyboard = [[$inButt]];
        $keyboard        = [ "inline_keyboard" => $inline_keyboard ];

        $reply = json_encode($keyboard);
    }

    $url = 'https://api.telegram.org/bot'.TOKEN.'/sendMessage?disable_web_page_preview=true&chat_id='.$chat_id.'&parse_mode=Markdown&text='. $msg ."&reply_markup=".$reply ;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);

}

