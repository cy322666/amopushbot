<?php

function amoCRMCurl($data = array())
{
	$curl=curl_init(); #Сохраняем дескриптор сеанса cURL

	#Устанавливаем необходимые опции для сеанса cURL
	curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl,CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
	curl_setopt($curl,CURLOPT_URL, $data['link']);

	if (isset( $data['postfields'] )) {
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data['postfields']));
	}
	curl_setopt($curl,CURLOPT_HEADER, false);
	curl_setopt($curl,CURLOPT_COOKIEFILE, __DIR__.'/cookie.txt');
	curl_setopt($curl,CURLOPT_COOKIEJAR, __DIR__.'/cookie.txt');
	curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);

	$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
	$code=curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	return json_decode($out, true);
}

/**
 * Проверяет каждое value на совпадение
 *
 * @param $check
 * @param $value
 *
 * При совпадении пушит в чат телеграм сообщение
 */
function checkLead($check, $value)
{
	if ($check == 'start')
	{
		$id = file_get_contents('check.txt');
		if ( $value == $id )
		{
			sendMessage('...ДУБЛЬ ОБРАЩЕНИЯ');
			exit;
		}
	}
	else file_put_contents('check.txt', $value);
}

/**
 * Авторизация в amoCRM
 *
 * @param array $amo_user - массив с почтой и ключом аккаунта
 * @return mixed
 *
 * Возвращает массив от авторизации
 */
function authAmoCRM($amo_user, $subdomain, $chat_id, $msg_rep)
{
	unset($curldata);
	$curldata['link'] = 'https://'.$subdomain .'.amocrm.ru/private/api/auth.php?type=json';
	$curldata['postfields'] = $amo_user;
	$Response = amoCRMCurl($curldata);

	if (!$Response) {
		sendMessage($chat_id, $msg_rep['109'], '', '', '');
		exit;
	}
}

/**
 * Принимает массив
 *
 * @param $value    - массив
 *
 * Печатает его в читабельном виде
 */
function arrayPrint($array)
{
	echo '<pre>'; print_r($array); echo '</pre>';
}


function setWebhook ($chat_id, $subdomain, $array, $msg_rep)
{
	$curldata['link'] = 'https://'.$subdomain.'.amocrm.ru/api/v2/webhooks/subscribe';
	$curldata['postfields'] = $array;
	$Response = amoCRMCurl($curldata);

	arrayPrint($curldata);
	arrayPrint($Response);

	if(!$Response) sendMessage($chat_id, $msg_rep['109'], '', '', '');

}



?>
