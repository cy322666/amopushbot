<?php

function query($db, $query, $chat_id, $msg_rep)
{
    $connect = mysqli_connect($db['host'],$db['user'],$db['pass'],$db['name']);

    if ($connect)
    {
        $result = mysqli_query($connect, $query);
        $result = mysqli_fetch_assoc($result);

        arrayPrint($result);

        if( !$result )
        {
            sendMessage($chat_id, $msg_rep['109'], '', '', '');
            mysqli_close($connect);

            return false;

        } else
        {
            mysqli_close($connect);

            return $result;
        }


    } else
    {
        sendMessage($chat_id, $msg_rep['109'], '', '', '');
        return false;
    }
}